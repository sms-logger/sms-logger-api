const express = require("express");

const controllers = require("../controllers/log.controllers");

const Router = express.Router();

Router.post('/new', controllers.NewLog);
Router.get('/logs', controllers.FetchLogs);
Router.get('/log/:id', controllers.FetchLog);

module.exports = Router;