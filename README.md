# Kavenegar API

This is a **MERN** project that we are going to learn about **Kavenegar SMS System**.

## Tech

### Back-End

API is created with **ExpressJs**.

### Front-End

Front-End is a **Reactjs** application.

[Link to ReactJs](https://gitlab.com/kavenegar-sms-system-sample/kavenegar-frontend)

### Database

We use **MongoDB** as our database.

## How to run

### First clone and install deps

```bash
$ git clone https://gitlab.com/kavenegar-sms-system-sample/kavenegar-api && cd kavenegar-api
$ npm i
```

### Then run application

```bash
$ npm test
```