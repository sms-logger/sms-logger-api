const express = require("express");

const LogRoutes = require("./routes/log.routes");

const app = express();

app.use('/log', LogRoutes);

module.exports = app;