const NewLog = (req, res) => {
    res.send("New");
}

const FetchLogs = (req, res) => {
    res.send("All");
}

const FetchLog = (req, res) => {
    res.send("One");
}

module.exports = {
    NewLog,
    FetchLogs,
    FetchLog,
}